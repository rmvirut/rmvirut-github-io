---
layout: homepage
title: Home
---

# Hi, I'm Kojo

I'm the owner of Diji Domain IT Services, a registerd IT consulting business. 
I freelance as an IT consultant. I've worked on various IT projects usually with
 new businesses that need help with IT from web development, hardware 
 recommendation and sourcing to conducting business analysis to propose 
 solutions.

I'm currently working full-time as sales associate with the Source Bell 
Electronics where I peform similar duties but for individuals looking to bring 
some convenience into their lives with tech.

I'm actively looking for new opportunities in any IT related fields or projects.
 And feel free to contact me if you're looking to hire a freelancer for 
 something interesting