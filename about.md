---
title: About Me
layout: default
---

# First Thing To Know About Me

This site is meant to be a professional portfolio and project showcase for my
career as an IT Consultant. But I also like to let people know me for more than
how good I am at analysing and resolving business problems using tech (nice healine, huh?).

I'll start with my name. My name's Kojo Acquah and I was born and raised in Ghana
but currently reside in Canada as of September 9, 2014. However, since meeting
Jesus Christ and having my whole life transformed I like to introduce myself as
a man born and raised in Ghana, African by heritage, Canadian on paper (soon),
Heaven-bound by promise (thank you Jesus) and above all else, a friend of God.

Some might consider this crazy, but I welcome and respect this sentiment and most
importantly welcome any and all who would like to hear more of my testimony as
a young Chrisitan teen that had his own personal encounter with the Lord. But
enough about that (sadly) and unto me as a professional.

## Moving On

Personally there's too much to put on this page. I've lived a rich a and extremely
fulfilling life, especially regarding the fact that I'm (at the time of publishing)
only 24 years old (born '95). I've met people from around the world thanks to my
dad's line of work and also gained exposure to even more cultures besides my own
before and after relocating to Canada. It's pretty exciting. Sadly, I might also
be on the autism spectrum thus I'm currently looking into seeing a professional
at some point

## The Timeline

This is just to showcase where and when I've been in life. I'll add more detail and hopefully style, as time goes on. But enjoy the trip down memory lane.

May '95 - Born to Mr & Mrs Acquah. From what I hear it was fast and I stared at people a lot

April 2010 - Graduated UMaT Basic School; Ranked 2nd in the Municipality

November 2010 - Enrolled in Mfantsipim Senior High School to study Science and Elective Maths

May 2013 - Graduated from Mfantsipim School with my Grade 12. Also the year I decided I wanted to go into IT and not Med School.

November 2013 - Got admitted into both Saint Mary's University, Halifax and York Univeristy, Ontario. Opted to defer going to school due to timing

Year of 2014 - Enrolled into IPMC to study IT including office applciation & web programming (JavaScript, HTMl, CSS). Got my driver's license and also relocated to Accra temporarily to take SAT's

September 8, 2014 - Hoped unto a KLM flight from Accra Ghana and set of for the land of Maple Syrup, beavers and Mounties. I had decided to go to Saint Mary's University to pursue Computer Science.

September 9, 2014 -  I instantly regret my decision as the Weather changes 5 times in 2 hours. What kind of country is this?

September 9-10, 2014 - Regret replaced with exhiliriation as we play soccer for close to 8hrs during welcome week. I wore no shoes.

August 2015 - Fist time at ANFGC Halifax. This would be the start of a whole new life! Although to be honest the food brought me here (thank you roomie)

September 2016 - Started workign as a self-employed IT consultant doing everything from PC troubleshooting and repair to designing websites and archiving corporate emails

October 2019 - I made it onto the Dean's List! Wohooo! Thank you Jesus!

April 2019 - Graduated from school at last with a BSc in Computer Science. But life throws one heck of a curve ball and by God's grace I make it, barely

October 2019 - Officially relocate to Stellarton, NS to help with CFC's IT and anything else they'd need and also to "forget" the past. Oh, and I also got employed at the Source

December 2019 - Got a performance award at the Source! "Highest number of mobility accessories sold in December, 2019 for the District!"

January 2020 - Got a promotion to full-time at the Source to help me with my PR application the future

To be cont'd
